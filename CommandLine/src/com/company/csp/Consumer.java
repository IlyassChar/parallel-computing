package com.company.csp;

import java.io.PrintStream;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.activemq.ActiveMQConnectionFactory;

public class Consumer
{
    private static String mUrl = "failover:(tcp://localhost:8161?trace=true)";
    private static String mQueueName = "activeMQ_02";

    public static void main(String[] args) throws JMSException
    {
        System.out.println("Consumer started.\n");

        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(mUrl);
        Connection connection = connectionFactory.createConnection();
        connection.start();

        Session session = connection.createSession(false, 1);
        Destination destination = session.createQueue(mQueueName);

        System.out.printf("Retrieving Message from %s\n", new Object[] { mQueueName });
        MessageConsumer consumer = session.createConsumer(destination);
        Message message = consumer.receive();

        if ((message instanceof TextMessage)) {
            TextMessage textMessage = (TextMessage)message;

            System.out.println(textMessage.getText());
        }

        connection.close();
    }

    private static void printList(int[] numberList, boolean isSorted)
    {
        System.out.printf("Number list %s sorting:\n", new Object[] { isSorted ? "after" : "before" });
        for (int i : numberList)
            System.out.println(i);
    }
}