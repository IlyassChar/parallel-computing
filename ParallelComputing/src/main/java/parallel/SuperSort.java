package parallel;

import synced.InsertionSort;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("Duplicates")
public class SuperSort {
    public static void sort(int[] input) {
        System.out.println();
        System.out.println("Synced:         (" + input.length + ")");
        // get hash codes
        long startTime = System.currentTimeMillis();

        final int[] code = hash(input); // create and initialize buckets to ArrayList: O(n)
        List<List<Integer>> buckets = new ArrayList<>(code[1]);
        for (int i = 0; i < code[1]; i++) {
            buckets.add(new ArrayList<>());
        }

//         distribute data into buckets: O(n)
        for (int i : input) {
            buckets.get(hash(i, code)).add(i);
        }
        // sort each bucket O(n)
        for (int i = 0; i < buckets.size(); i++) {
            buckets.set(i, InsertionSort.sort2(buckets.get(i)));
        }

        int ndx = 0;
        // merge the buckets: O(n)
        for (List bucket : buckets) {
            for (Object v : bucket) {
                input[ndx++] = (int) v;
            }
        }
        long estimatedTime = System.currentTimeMillis() - startTime;
        System.out.println("Elapsed time (Synced):  " + estimatedTime);
        System.out.println();
    }

    private static int[] hash(int[] input) {
        int m = input[0];
        for (int i = 1; i < input.length; i++) {
            if (m < input[i]) {
                m = input[i];
            }
        }

        int[] result = new int[]{m, (int) Math.sqrt(input.length)};
        return result;
    }

    private static int hash(int i, int[] code) {
        return (int) ((double) i / code[0] * (code[1] - 1));
    }

}



