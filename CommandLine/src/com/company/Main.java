package com.company;

import java.util.Random;

public class Main
{
    public static void main(String[] args)
    {
        buildResult(10000);
        buildResult(20000);
        buildResult(40000);
        buildResult(80000);
        buildResult(160000);
    }

    private static void buildResult(int count) {
        Integer[] result = createSet(count);

//        ThreadedBucketSort.parallelSort(result, 2);
//        ThreadedBucketSort.parallelSort(result, 4);
//        ThreadedBucketSort.parallelSort(result, 6);
//        ThreadedBucketSort.parallelSort(result, 8);
//        ThreadedBucketSort.parallelSort(result, 10);
//        ThreadedBucketSort.parallelSort(result, 12);
//        ThreadedBucketSort.parallelSort(result, 14);
//        ThreadedBucketSort.parallelSort(result, 16);

        SyncBucketSort.sort(result, 2);
        SyncBucketSort.sort(result, 4);
        SyncBucketSort.sort(result, 6);
        SyncBucketSort.sort(result, 8);
        SyncBucketSort.sort(result, 10);
        SyncBucketSort.sort(result, 12);
        SyncBucketSort.sort(result, 14);
        SyncBucketSort.sort(result, 16);
    }

    private static Integer[] createSet(int totalCount)
    {
        Integer[] result = new Integer[totalCount];
        Random random = new Random();

        for (int i = 0; i < totalCount; ) {
            result[i] = Integer.valueOf(random.nextInt(100000) + 1);

            i++;
        }

        return result;
    }
}