package csp;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;
import java.util.Arrays;
import java.util.Random;

public class Producer
{
    private static String mUrl = "failover:(tcp://localhost:8161?trace=true)";
    private static String mQueueTo = "activeMQ_01";
    private static String mQueueFrom = "activeMQ_02";

    public static void main(String[] args) throws JMSException {
        Integer[] numberList = createSet(10);

        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(mUrl);
        Connection connection = connectionFactory.createConnection();
        connection.start();
        long startTime = System.currentTimeMillis();

        Session session = connection.createSession(false, 1);
        Destination destination = session.createQueue(mQueueTo);
        MessageProducer producer = session.createProducer(destination);
        producer.setDeliveryMode(1);

        TextMessage message = session.createTextMessage(Arrays.toString(numberList));
        producer.send(message);

        Destination destination_fromQueue = session.createQueue(mQueueFrom);
        MessageConsumer consumer = session.createConsumer(destination_fromQueue);
        consumer.setMessageListener(message1 -> {
            Integer[] sortedArray = null;

            if ((message1 instanceof TextMessage))
            {
                TextMessage textMessage = (TextMessage) message1;
                String strArrayFromQueue = null;
                String[] integers = null;
                try {
                    strArrayFromQueue = textMessage.getText();

                    integers = strArrayFromQueue
                            .replaceAll("\\[", "")
                            .replaceAll("\\]", "")
                            .replaceAll("\\s", "")
                            .split(",");
                }
                catch (JMSException e) {
                    e.printStackTrace();
                }

                sortedArray = new Integer[integers.length];

                System.out.println("Turning String Array into Integer Array.\n");

                for (int i = 0; i < sortedArray.length; i++)
                    try {
                        sortedArray[i] = Integer.valueOf(Integer.parseInt(integers[i]));
                    }
                    catch (NumberFormatException localNumberFormatException) {
                    }
            }
            else {
                System.err.println("Failed to get Message!");
            }

            try
            {
                System.out.printf("ACTIVEMQ DONE IN: %s ms", (System.currentTimeMillis() - startTime) );
                connection.close();
            } catch (JMSException e) {
                e.printStackTrace();
            }
        });
    }

    private static void printList(int[] numberList, boolean isSorted)
    {
//        System.out.printf("Number list %s sorting:\n", new Object[] { isSorted ? "after" : "before" });
        for (int i : numberList)
            System.out.println(i);
    }

    private static Integer[] createSet(int totalCount)
    {
        Integer[] result = new Integer[totalCount];
        Random random = new Random();

        for (int i = 0; i < totalCount; ) {
            result[i] = Integer.valueOf(random.nextInt(100000) + 1);

            i++;
        }

        return result;
    }
}