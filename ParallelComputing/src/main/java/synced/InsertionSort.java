package synced;

import java.util.List;

public class InsertionSort {
//    static <T extends Comparable<T>> T[] sort(T[] array) {
//
//        System.out.println("ORIGINAL BUCKET ARRAY: " + Arrays.toString(array));
//        for (int i = 1; i < array.length; i++) {
//            T item = array[i];
//            int indexHole = i;
//            while (indexHole > 0 && array[indexHole - 1].compareTo(item) > 0) {
//                array[indexHole] = array[--indexHole];
//            }
//            array[indexHole] = item;
//        }
//        System.out.println("SORTED BUCKET ARRAY: " + Arrays.toString(array));
//
//        return array;
//    }

    public static List<Integer> sort2(List arr) {
        int n = arr.size();

        for (int i = 1; i < n; ++i) {
            int key = (int) arr.get(i);
            int j = i - 1;

            while (j >= 0 && (int) arr.get(j) > key) {
                arr.set(j + 1, arr.get(j));
                j = j - 1;
            }
            arr.set(j + 1, key);
        }
        return arr;
    }

    public static Integer[] sort(Integer arr[]) {
        int n = arr.length;
        for (int i = 1; i < n; ++i) {
            int key = arr[i];
            int j = i - 1;

            while (j >= 0 && arr[j] > key) {
                arr[j + 1] = arr[j];
                j = j - 1;
            }
            arr[j + 1] = key;
        }
        return arr;
    }
}
