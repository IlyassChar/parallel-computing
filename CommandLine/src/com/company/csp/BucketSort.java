package com.company.csp;

import com.company.SyncBucketSort;
import java.io.PrintStream;
import java.util.Arrays;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.activemq.ActiveMQConnectionFactory;

public class BucketSort
{
    private static String mUrl = "failover:(tcp://localhost:8161?trace=true)";

    private static String mRetrieverUrl = "activeMQ_01";
    private static String mDestinationUrl = "activeMQ_02";

    public static void main(String[] args) throws JMSException {
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(mUrl);
        Connection connection = connectionFactory.createConnection();
        connection.start();

        Session session = connection.createSession(false, 1);
        Destination destination_fromQueue = session.createQueue(mRetrieverUrl);
        MessageConsumer consumer = session.createConsumer(destination_fromQueue);
        Message message = consumer.receive();

        Integer[] arrayToSort = null;

        if ((message instanceof TextMessage))
        {
            TextMessage textMessage = (TextMessage)message;
            String strArrayFromQueue = textMessage.getText();

            String[] integers = strArrayFromQueue
                    .replaceAll("\\[", "")
                    .replaceAll("\\]", "")
                    .replaceAll("\\s", "")
                    .split(",");

            arrayToSort = new Integer[integers.length];

            System.out.println("Turning String Array into Integer Array.\n");

            for (int i = 0; i < arrayToSort.length; i++)
                try {
                    arrayToSort[i] = Integer.valueOf(Integer.parseInt(integers[i]));
                }
                catch (NumberFormatException localNumberFormatException) {
                }
        }
        else {
            System.err.println("Failed to get Message!");
        }

        Destination destination_toQueue = session.createQueue(mDestinationUrl);
        MessageProducer producer = session.createProducer(destination_toQueue);

        SyncBucketSort.sort(arrayToSort);

        String stringForConsumer = Arrays.toString(arrayToSort);

        TextMessage messageTo = session.createTextMessage(stringForConsumer);
        producer.send(messageTo);
        connection.close();
    }
}