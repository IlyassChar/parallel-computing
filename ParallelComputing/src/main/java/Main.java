import parallel.ParallelSuperSort;
import parallel.SuperSort;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        buildResult(10000);
        buildResult(20000);
        buildResult(40000);
        buildResult(80000);
        buildResult(100000);
    }

    private static void buildResult(int count) {
        int[] result = createSet(count);
        System.out.println("-------SYNCED-------");
        SuperSort.sort(result.clone());
        System.out.println("--------------------");

        ParallelSuperSort.parallelSort(result.clone(), 2);
        ParallelSuperSort.parallelSort(result.clone(), 4);
        ParallelSuperSort.parallelSort(result.clone(), 6);
        ParallelSuperSort.parallelSort(result.clone(), 8);


    }

    private static int[] createSet(int totalCount) {
        int[] result = new int[totalCount];
        Random random = new Random();

        for (int i = 0; i < totalCount; ) {
            result[i] = Integer.valueOf(random.nextInt(100000) + 1);

            i++;
        }

        return result;
    }
}