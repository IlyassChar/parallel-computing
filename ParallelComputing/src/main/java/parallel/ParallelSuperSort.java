package parallel;

import com.google.common.collect.Lists;
import com.google.common.math.IntMath;
import synced.InsertionSort;

import java.lang.reflect.Array;
import java.math.RoundingMode;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SuppressWarnings("Duplicates")
public class ParallelSuperSort {
    public static void parallelSort(int[] input, int threads) {
        System.out.println("---------------------------");
        System.out.println("Number of items:    " + input.length);
        System.out.println("Number of threads:  " + threads);
        // get hash codes
        long startTime = System.currentTimeMillis();

        final int[] code = hash(input);
        // create and initialize buckets to ArrayList: O(n)
        List<List<Integer>> buckets = new ArrayList<>(code[1]);

        for (int i = 0; i < code[1]; i++) {
            buckets.add(new ArrayList<>());
        }

//         distribute data into buckets: O(n)
        for (int i : input) {
            buckets.get(hash(i, code)).add(i);
        }
        int partitionSize = IntMath.divide(buckets.size(), threads, RoundingMode.UP);
        List<List<List<Integer>>> result = Lists.partition(buckets, partitionSize);

        Map<Integer, Thread> runnables = new HashMap<>();


        for (int i = 0; i < result.size(); i++) {
            runnables.put(i + 1, createThread(result.get(i), input, i + 1));
        }

        //Create merging thread
        runnables.put(threads + 1, createMergingThread(result, input));

        for (Map.Entry<Integer, Thread> entry : runnables.entrySet()) {
            entry.getValue().start();
        }

        for (Map.Entry<Integer, Thread> entry : runnables.entrySet()) {
            try {
                entry.getValue().join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        long estimatedTime = System.currentTimeMillis() - startTime;
        System.out.println("Elapsed time:       " + estimatedTime);
        System.out.println();

    }

    private static Thread createThread(List<List<Integer>> partition, int[] input, int currentThread) {

        Thread thread = new Thread(() -> {
            for (int i = 0; i < partition.size(); i++) {
                partition.set(i, InsertionSort.sort2(partition.get(i)));
            }
        });
        thread.setName("Thread " + (currentThread));

        return thread;
    }

    private static Thread createMergingThread(List<List<List<Integer>>> buckets, int[] input) {

        return new Thread(() -> {
            // get hash codes

            List<List<Integer>> result = new ArrayList<>();
            buckets.forEach(result::addAll);

            int ndx = 0;
            for (List bucket : result) {
                for (Object v : bucket) {
                    input[ndx++] = (int) v;
                }
            }
        });
    }


    private static int[] hash(int[] input) {
        int m = input[0];
        for (int i = 1; i < input.length; i++) {
            if (m < input[i]) {
                m = input[i];
            }
        }
        return new int[]{m, (int) Math.sqrt(input.length)};
    }

    private static int hash(int i, int[] code) {
        return (int) ((double) i / code[0] * (code[1] - 1));
    }

}



