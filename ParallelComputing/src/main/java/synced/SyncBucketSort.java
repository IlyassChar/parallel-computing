package synced;

import synced.InsertionSort;

import java.util.ArrayList;
import java.util.Arrays;

public class SyncBucketSort
{
    private static final int DEFAULT_BUCKET_SIZE = 5;

    public static void sort(Integer[] array)
    {
        sort(array, 5);
    }

    public static void sort(Integer[] original, int bucketSize)
    {
        Integer[] list = Arrays.copyOf(original, original.length);
        System.out.println("SYNCED BUCKETSORT");
        long start = System.currentTimeMillis();

        Integer highest = list[0];
        Integer lowest = list[0];

        for (Integer student : list) {
            if (highest == null) {
                throw new NullPointerException();
            }
            if (highest.compareTo(student) < 0)
                highest = student;
            else if (lowest.compareTo(student) > 0) {
                lowest = student;
            }

        }

        ArrayList<ArrayList<Integer>> buckets = new ArrayList(bucketSize);
        for (int i = 0; i < bucketSize; i++)
            buckets.add(new ArrayList());
        int bucketNumber;
        for (int i = 0; i < list.length; i++) {
            bucketNumber = bucketForNumber(list[i], lowest, highest, bucketSize);
            buckets.get(bucketNumber).add(list[i]);
        }

        int listIndex = 0;
        System.out.println("Buckets>" + buckets.size());


        for (ArrayList<Integer> bucket : buckets) {
            Integer[] bucketArray = new Integer[bucket.size()];
            bucketArray = bucket.toArray(bucketArray);

            InsertionSort.sort(bucketArray);

            for (Integer item : bucket) {
                list[(listIndex++)] = item;
            }
        }
        long elapsedTimeMillis = System.currentTimeMillis() - start;
        System.out.println("Elapsed time: " + elapsedTimeMillis);
        System.out.println("------------------------------------------------------------------------>");
    }

    private static int bucketForNumber(Integer number, Integer listMin, Integer listMax, Integer numBuckets)
    {
        Long difference = Long.valueOf(listMax.intValue() - listMin.intValue());
        int increment = (int)Math.ceil(difference.longValue() / numBuckets.intValue());
        increment = increment <= 0 ? 1 : increment;
        int bucket = number.intValue() / increment;
        if (bucket >= numBuckets.intValue())
            bucket = numBuckets.intValue() - 1;
        else if (bucket < 0) {
            bucket = 0;
        }
        return bucket;
    }
}