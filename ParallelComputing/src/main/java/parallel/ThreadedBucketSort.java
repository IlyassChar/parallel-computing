package parallel;

import synced.InsertionSort;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

@SuppressWarnings("ALL")
public class ThreadedBucketSort {

    private static final int DEFAULT_BUCKET_SIZE = 5;

    public static void parallelSort(Integer[] array, int threadCount, int bucketSize) {
        Integer highest = array[0];
        Integer lowest = array[0];

        //Get highest and lowest grade in array
        for (Integer student : array) {
            if (highest == null) {
                throw new NullPointerException();
            }
            if (highest.compareTo(student) < 0) {
                highest = student;
            } else if (lowest.compareTo(student) > 0) {
                lowest = student;
            }
        }

        //Determine bucket count that should be used
        int bucketCount = (highest - lowest) / DEFAULT_BUCKET_SIZE + 1;
        List<List<Integer>> buckets = new ArrayList<>(bucketCount);


        for (int i = 0; i < bucketCount; i++) {
            buckets.add(new ArrayList<>());
        }

        for (Integer anArray : array) {
            buckets.get((anArray - lowest) / bucketSize).add(anArray);
        }

        AtomicInteger currentBucket = new AtomicInteger(0);

        Map<Integer, Thread> runnables = new HashMap<>();
        Map<Integer, Integer[]> results = new HashMap<>();

        for (int i = 0; i < buckets.size(); i++) {

            List<Integer> bucket = buckets.get(i);
            final int current = i;

            Thread thread = new Thread(() -> {
                System.out.println(Thread.currentThread().getName() + " started.");
                Integer[] bucketArray = new Integer[bucket.size()];

                bucketArray = bucket.toArray(bucketArray);
                Integer[] res = InsertionSort.sort(bucketArray);

                results.put(current, res);
            });

            thread.setName("Thread " + (i + 1));

            runnables.put(currentBucket.getAndIncrement(), thread);
        }

        for (Map.Entry<Integer, Thread> entry : runnables.entrySet()) {
            entry.getValue().start();
        }

        for (Map.Entry<Integer, Thread> entry : runnables.entrySet()) {
            try {
                entry.getValue().join();
                System.out.println("Joined thread " + entry.getKey());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Integer[] finalresult = new Integer[0];

        for (int i = 0; i < bucketCount; i++) {
            Integer[] temp = results.get(i);

            Integer[] both = Arrays.copyOf(finalresult, finalresult.length + temp.length);
            System.arraycopy(temp, 0, both, finalresult.length, temp.length);

            finalresult = both;
        }

    }


}